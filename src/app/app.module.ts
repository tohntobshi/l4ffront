import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SessionService } from './session.service';
import { AuthorizationFormComponent } from './authorization-form/authorization-form.component';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { RecursiveCommentsComponent } from './recursive-comments/recursive-comments.component';
import { CommentComponent } from './comment/comment.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { AttachmentFormComponent } from './attachment-form/attachment-form.component';
import { CreatePostFormComponent } from './create-post-form/create-post-form.component';
import { DropFileDirective } from './attachment-form/drop-file.directive';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ProfileComponent } from './profile/profile.component';
import { UsersComponent } from './users/users.component';
import { AttachmentService } from './attachment-form/attachment.service';
import { CommentsComponent } from './comments/comments.component';
import { SearchComponent } from './search/search.component';
import { AutofocusDirective } from './autofocus.directive';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { VerificationComponent } from './verification/verification.component';
import { RecreationPasswordComponent } from './recreation-password/recreation-password.component';
import { RecreationPasswordFormComponent } from './recreation-password-form/recreation-password-form.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { BackendService } from './backend.service';
import { EditComponent } from './edit/edit.component';
import { MyDateTime } from './mydatetime.pipe';
import { NotificationComponent } from './notification/notification.component';
import { FeedComponent } from './feed/feed.component';
import { FavPostsComponent } from './fav-posts/fav-posts.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { FavCommentsComponent } from './fav-comments/fav-comments.component';
import { MessagesComponent } from './messages/messages.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { Page404Component } from './page404/page404.component';
import { LanguageService } from './language.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AttachmentComponent } from './attachment/attachment.component';
import { WindowScrollService } from './window-scroll.service';
import { SharedService } from './shared.service';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ModalPictureComponent } from './modal-picture/modal-picture.component';
import { AboutComponent } from './about/about.component';
import { InnerApiService } from './inner-api.service';
import { EComponent } from './e/e.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { VkRegistrationComponent } from './vk-registration/vk-registration.component';
import { TradRegistrationComponent } from './trad-registration/trad-registration.component';
import { GoogleRegistrationComponent } from './google-registration/google-registration.component';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        SignUpComponent,
        AuthorizationFormComponent,
        PostsComponent,
        PostComponent,
        RecursiveCommentsComponent,
        CommentComponent,
        CommentFormComponent,
        AttachmentFormComponent,
        CreatePostFormComponent,
        DropFileDirective,
        ProfileComponent,
        UsersComponent,
        CommentsComponent,
        SearchComponent,
        AutofocusDirective,
        ProfileSettingsComponent,
        VerificationComponent,
        RecreationPasswordComponent,
        RecreationPasswordFormComponent,
        EditComponent,
        MyDateTime,
        NotificationComponent,
        FeedComponent,
        FavPostsComponent,
        SubscriptionsComponent,
        FavCommentsComponent,
        MessagesComponent,
        ComplaintsComponent,
        Page404Component,
        AttachmentComponent,
        ModalPictureComponent,
        AboutComponent,
        EComponent,
        VkRegistrationComponent,
        TradRegistrationComponent,
        GoogleRegistrationComponent
    ],
    entryComponents: [
        NotificationComponent,
        ModalPictureComponent,
        AboutComponent,
        AuthorizationFormComponent,
        RecreationPasswordFormComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        CollapseModule.forRoot(),
        ModalModule.forRoot(),
        ButtonsModule.forRoot(),
        BsDropdownModule.forRoot(),
        AppRoutingModule,
        AlertModule.forRoot(),
        RecaptchaModule.forRoot(),
        BrowserAnimationsModule,
        PopoverModule.forRoot(),
        AccordionModule.forRoot()
    ],
    providers: [
        SessionService,
        AttachmentService,
        BackendService,
        LanguageService,
        WindowScrollService,
        SharedService,
        InnerApiService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
