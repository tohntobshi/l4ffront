import { Component, HostListener } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-modal-picture',
    templateUrl: './modal-picture.component.html',
    styleUrls: ['./modal-picture.component.css']
})
export class ModalPictureComponent {

    constructor(
        private modalRef: BsModalRef
    ) { }

    src: string;
    @HostListener('click')
    hide() {
        this.modalRef.hide();
    }

}
