import { Component, OnInit, HostBinding } from '@angular/core';
import { BackendService } from '../backend.service';
import { User } from '../simple-classes/user';
import { routingAnimation, fancyList, fadeScale } from '../animations';
import { Title } from '@angular/platform-browser';
import { LanguageService } from '../language.service';
import { SharedService } from '../shared.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css'],
    animations: [ routingAnimation, fancyList, fadeScale ]
})
export class UsersComponent implements OnInit {

    constructor(
        private backend: BackendService,
        private title: Title,
        public lang: LanguageService,
        private shared: SharedService
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    upic(user) {
        if(user.userpic) {
            return "/userpics/" + user.userpic + ".jpg";
        } else {
            return "/userpics/default.jpg";
        }
    }
    users: User[];
    ngOnInit() {
        this.title.setTitle(this.lang.lang.top_users);
        let form = {
            query: "users"
        }
        this.backend.query(form).subscribe(response => {
            if(response.result) {
                this.users = response.data.users;
                this.ready = true;
            }
        })
    }
    showAbout() {
        this.shared.showSiteInfo();
    }
    ready: boolean;
}
