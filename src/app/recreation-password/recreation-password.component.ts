import { Component, OnInit, ViewChild, HostBinding } from '@angular/core';
import { BackendService } from '../backend.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LanguageService } from '../language.service';
import { routingAnimation } from '../animations';
import { Title } from '@angular/platform-browser';
import { SharedService } from '../shared.service';

@Component({
    selector: 'app-recreation-password',
    templateUrl: './recreation-password.component.html',
    styleUrls: ['./recreation-password.component.css'],
    animations: [ routingAnimation ]
})
export class RecreationPasswordComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private backend: BackendService,
        public lang: LanguageService,
        private title: Title,
        public shared: SharedService
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        this.title.setTitle(this.lang.lang.create_new_pwd);
        this.user_id = this.route.snapshot.paramMap.get('uid');
        this.recreation_password = this.route.snapshot.paramMap.get('pwd');
        let uid_regex = /^[0-9]+$/;
        let pwd_regex = /^[0-9A-Za-z]{128}$/;
        if (!uid_regex.test(this.user_id) || !pwd_regex.test(this.recreation_password)) {
            this.router.navigate(['']);
            return;
        }
        this.form = new FormGroup({
            'pwd': new FormControl('', [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(32)
            ]),
            'pwd2': new FormControl('', Validators.required)
        });
    }
    @ViewChild('captchaRef') captchaRef;
    done = false;
    fail = false;
    user_id;
    recreation_password;
    form: FormGroup;
    comparePasswords() {
        if (!this.form) {
            return false;
        }
        return this.pwd.value == this.pwd2.value ? false : true;
    }
    get pwd() { return this.form.get('pwd') }
    get pwd2() { return this.form.get('pwd2') }
    submit(captcha: string) {
        let form = {
            query: "recreate_password_end",
            user_id: this.user_id,
            recreation_password: this.recreation_password,
            new_password: this.pwd.value,
            captcha: captcha
        }
        this.backend.query(form).subscribe(response => {
            this.captchaRef.reset();
            if (response.result) {
                this.done = true;
            } else {
                this.fail = true;
            }
            setTimeout(() => {
                this.router.navigate(['']);
            }, 5000);
        })
    }
}
