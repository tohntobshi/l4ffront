import { Component, OnInit, HostListener } from '@angular/core';
import { SessionService } from './session.service';
import { LanguageService } from './language.service';
import { SharedService } from './shared.service';
import { Meta } from '@angular/platform-browser';
import { InnerApiService } from './inner-api.service';

declare var gapi: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor(
        private lang: LanguageService,
        private session: SessionService,
        private shared: SharedService,
        private meta: Meta,
        private inner: InnerApiService
    ) { }
    ngOnInit() {
        this.lang.getLang();
        this.meta.addTag({name: "description", content: this.lang.lang.l4f_desc1});
        this.session.checkSession();
        this.session.checkMessages();
        setInterval(() => {
            this.session.checkMessages();
        }, 15000);
        setInterval(() => {
            this.session.checkSession();
        }, 5*60*1000);
        if (!localStorage.getItem("infoShown")) {
            this.shared.showSiteInfo();
            localStorage.setItem("infoShown", '1');
        }
    }
    @HostListener('dragenter')
    onDragenter() {
        this.inner.api.next({
            query: 'drag'
        })
    }
}
