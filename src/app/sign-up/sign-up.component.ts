import { Component, OnInit, HostBinding } from '@angular/core';
import { LanguageService } from '../language.service';
import { routingAnimation } from '../animations';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css'],
    animations: [ routingAnimation ]
})
export class SignUpComponent implements OnInit {

    constructor(
        public lang: LanguageService,
        private title: Title,
        private route: ActivatedRoute
    ){}

    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        this.title.setTitle(this.lang.lang.sign_up);
        this.regtype = this.route.snapshot.paramMap.get('reg');
    }
    regtype: string;
}
