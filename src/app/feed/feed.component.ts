import { Component, OnInit, HostBinding } from '@angular/core';
import { routingAnimation } from '../animations';
import { LanguageService } from '../language.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-feed',
    templateUrl: './feed.component.html',
    styleUrls: ['./feed.component.css'],
    animations: [ routingAnimation ]
})
export class FeedComponent implements OnInit {

    @HostBinding('@fadeInOut') fadeInOut = true;
    constructor(
        public lang: LanguageService,
        private title: Title
    ) { }

    ngOnInit() {
        this.title.setTitle(this.lang.lang.feed);
    }
}
