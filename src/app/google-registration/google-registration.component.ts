import { Component, OnInit, OnDestroy } from '@angular/core';
import { LanguageService } from '../language.service';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

declare var gapi: any;

@Component({
    selector: 'app-google-registration',
    templateUrl: './google-registration.component.html',
    styleUrls: ['./google-registration.component.css']
})
export class GoogleRegistrationComponent implements OnInit, OnDestroy {

    constructor(
        public lang: LanguageService,
        private backend: BackendService,
        private router: Router,
        private shared: SharedService
    ) {}
    done = false;
    already_registered = false;
    form: FormGroup;
    ngOnInit() {
        this.check_interval = setInterval(() => {
            if (!this.id_token) {
                return;
            }
            this.submit2(this.id_token);
            this.id_token = undefined;
        }, 300);
        this.form = new FormGroup({
            'username': new FormControl('', [
                Validators.required,
                Validators.minLength(4),
                Validators.pattern(/^[a-zA-Z0-9а-яА-ЯёЁ_\-]*$/),
                Validators.maxLength(32)
            ], this.validateUsername.bind(this))
        });
    }
    ngOnDestroy() {
        clearInterval(this.check_interval);
    }
    get username() { return this.form.get('username') }
    submit() {
        if (gapi.auth2) {
            gapi.auth2.authorize({
                client_id: this.shared.google_client_id,
                scope: 'openid',
                response_type: 'id_token'
            }, response => {
                if (response.error) {
                    return;
                }
                this.id_token = response.id_token;
            })
        }
    }
    check_interval;
    id_token: string;
    validateUsername(control: AbstractControl) {
        return Observable.timer(2000).switchMap(() => {
            let form = {
                query: "check_username",
                username: control.value
            }
            return this.backend.query(form).map(res => {
                return res.result ? null : { usernameTaken: true };
            });
        });
    }
    submit2(id_token) {
        let form = {
            query: 'sign_up_google',
            username: this.username.value,
            id_token: id_token
        }
        this.backend.query(form).subscribe(r => {
            if (!r.result) {
                if(r.error && r.error === "already_registered") {
                    this.success(true);
                }
                return;
            }
            this.success();
        })
    }
    success(already_registered: boolean = false) {
        this.already_registered = already_registered;
        this.done = true;
        setTimeout(() => {
            this.router.navigate(['']);
        }, 5000);
    }
}
