import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService } from './language.service';

@Pipe({name: 'mydatetime'})
export class MyDateTime implements PipeTransform {
    constructor(
        private lang: LanguageService
    ) {}
    transform(value, mode: string) {
        let datetime;
        switch(mode) {
            case 'datetime':
                datetime = new Date(value.replace(/-/g, "/") + " GMT");
                let minutes = datetime.getMinutes().toString().length === 1 ? '0' + datetime.getMinutes() : datetime.getMinutes();
                return datetime.getDate() + " " + this.lang.lang.months[datetime.getMonth()] + " " + datetime.getFullYear() + " " + datetime.getHours() + ":" + minutes;
            case 'date':
                datetime = new Date(value.replace(/-/g, "/"));
                return datetime.getDate() + " " + this.lang.lang.months[datetime.getMonth()] + " " + datetime.getFullYear();
        }
    }
}
