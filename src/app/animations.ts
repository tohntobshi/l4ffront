import { animate, AnimationEntryMetadata, state, style, transition, trigger, group } from '@angular/core';

export const postAnimation: AnimationEntryMetadata =
    trigger('fadeAndSlide', [
        state('*', style({
            opacity: '1'
        })),
        transition('void => *', [
            style({
                opacity: '0',
                transform: 'translateY(30px)'
            }),
            animate('0.5s ease-out')
        ]),
        transition('* => void', animate('0.5s ease-in', style({
            opacity: '0',
            transform: 'translateY(30px)'
        })))
    ]);
export const routingAnimation: AnimationEntryMetadata =
    trigger('fadeInOut', [
        state('*', style({
            opacity: '1',
            display: 'block',
            position: 'absolute',
            width: '100%',
            top: '70px'
        })),
        transition('void => *', [
            style({
                opacity: '0',
                display: 'block',
                position: 'absolute',
                width: '100%'
            }),
            animate('0.5s ease-out')
        ]),
        transition('* => void', animate('0.2s ease-in', style({
            opacity: '0',
            display: 'block',
            position: 'absolute',
            width: '100%'
        })))
    ]);
export const slideFromTop: AnimationEntryMetadata =
    trigger('slideFromTop', [
        state('*', style({
            opacity: '1'
        })),
        transition('void => *', [
            style({
                opacity: '0',
                transform: 'translateY(-30px)'
            }),
            animate('0.5s ease-out')
        ]),
        transition('* => void', animate('0.5s ease-in', style({
            opacity: '0',
            transform: 'translateY(-30px)'
        }))),
    ]);
export const slideSeparately: AnimationEntryMetadata =
    trigger('slideSeparately', [
        state('*', style({
            transform: 'translateY(0px)',
            opacity: '1'
        })),
        transition('void => 1', [
            style({
                transform: 'translateY(20px)',
                opacity: '0'
            }),
            animate('1.5s ease-out')
        ]),
        transition('void => 2', [
            style({
                transform: 'translateY(60px)',
                opacity: '0'
            }),
            animate('1.5s ease-out')
        ]),
        transition('void => 3', [
            style({
                transform: 'translateY(100px)',
                opacity: '0'
            }),
            animate('1.5s ease-out')
        ]),
        transition('void => 4', [
            style({
                transform: 'translateY(140px)',
                opacity: '0'
            }),
            animate('1.5s ease-out')
        ]),
        transition('void => 5', [
            style({
                transform: 'translateY(180px)',
                opacity: '0'
            }),
            animate('1.5s ease-out')
        ]),
    ]);
export const fancyList: AnimationEntryMetadata =
    trigger('fancyList', [
        state('*', style({
            opacity: '1'
        })),
        transition('void => 0', [
            style({
                transform: 'translateY(10px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 1', [
            style({
                transform: 'translateY(20px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 2', [
            style({
                transform: 'translateY(30px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 3', [
            style({
                transform: 'translateY(40px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 4', [
            style({
                transform: 'translateY(50px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 5', [
            style({
                transform: 'translateY(60px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 6', [
            style({
                transform: 'translateY(70px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 7', [
            style({
                transform: 'translateY(80px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 8', [
            style({
                transform: 'translateY(90px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ]),
        transition('void => 9', [
            style({
                transform: 'translateY(100px)',
                opacity: '0'
            }),
            animate('1s ease-out')
        ])
    ]);
export const fade: AnimationEntryMetadata =
    trigger('fade', [
        state('*', style({
            opacity: '1'
        })),
        transition('void => *', [
            style({
                opacity: '0'
            }),
            animate('0.5s ease-out')
        ]),
        transition('* => void', animate('0.2s ease-in', style({
            opacity: '0'
        })))
    ]);
export const fadeScale: AnimationEntryMetadata =
    trigger('fadeScale', [
        transition('void => *', [
            style({
                opacity: '0',
                transform: 'scale(0.8)'
            }),
            animate('0.3s 1s ease-out', style({
                opacity: '1',
                transform: 'scale(1)'
            }))

        ])
    ]);
