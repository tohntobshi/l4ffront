import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BackendService } from '../backend.service';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { routingAnimation } from '../animations';

@Component({
    selector: 'app-verification',
    templateUrl: './verification.component.html',
    styleUrls: ['./verification.component.css'],
    animations: [ routingAnimation ]
})
export class VerificationComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private backend: BackendService,
        private session: SessionService,
        public lang: LanguageService
    ) { }
    success = false;
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        let verifiction_code = this.route.snapshot.paramMap.get('code');
        let check = /^[a-z0-9A-Z]{128}$/;
        if (!verifiction_code || !check.test(verifiction_code)) {
            this.router.navigate(['']);
            return;
        }
        let form = {
            query: 'verification',
            code: verifiction_code
        }
        this.backend.query(form).subscribe(response => {
            if (!response.result) {
                this.router.navigate(['']);
                return;
            }
            this.success = true;
            this.session.checkSession();
            setTimeout(() => {
                this.router.navigate(['']);
            }, 5000);
        })
    }

}
