import { Component, OnInit, HostBinding } from '@angular/core';
import { routingAnimation } from '../animations';
import { LanguageService } from '../language.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-fav-posts',
    templateUrl: './fav-posts.component.html',
    styleUrls: ['./fav-posts.component.css'],
    animations: [ routingAnimation ]
})
export class FavPostsComponent implements OnInit {

    constructor(
        public lang: LanguageService,
        private title: Title
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        this.title.setTitle(this.lang.lang.favorite + " - " + this.lang.lang.posts);
    }
}
