import { Component, OnInit, HostBinding } from '@angular/core';
import { routingAnimation } from '../animations';
import { LanguageService } from '../language.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-subscriptions',
    templateUrl: './subscriptions.component.html',
    styleUrls: ['./subscriptions.component.css'],
    animations: [ routingAnimation ]
})
export class SubscriptionsComponent implements OnInit {

    constructor(
        public lang: LanguageService,
        private title: Title
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        this.title.setTitle(this.lang.lang.subscriptions);
    }
}
