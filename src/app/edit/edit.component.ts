import { Component, EventEmitter, OnInit, Input, Output, ViewChild, TemplateRef, OnDestroy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { LanguageService } from '../language.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy {
    @Output() action = new EventEmitter();
    @Input() title: string;
    @Input() value: string;
    @ViewChild('modal') modal: TemplateRef<any>;
    modalRef: BsModalRef;
    ngOnInit() {
        this.text = this.value;
        this.modalRef = this.modalService.show(this.modal, {
            keyboard: false,
            backdrop: false,
            ignoreBackdropClick: true
        });
    }
    ngOnDestroy() {
        this.modalRef.hide();
    }
    constructor(
        private modalService: BsModalService,
        public lang: LanguageService
    ) {}
    text:string;
    onSubmit() {
        if (!this.text) {
            this.onCancel();
        }
        this.action.emit({
            result: true,
            text: this.text.trim()
        });
    }
    onCancel() {
        this.action.emit({
            result: false
        })
    }

}
