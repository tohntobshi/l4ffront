import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { LanguageService } from '../language.service';
import { routingAnimation, slideFromTop } from '../animations';
import { Title } from '@angular/platform-browser';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    animations: [ routingAnimation, slideFromTop ]
})
export class SearchComponent implements OnInit, OnDestroy {

    constructor(
        private route: ActivatedRoute,
        public lang: LanguageService,
        private title: Title,
        private inner: InnerApiService,
        private router: Router
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        this.title.setTitle(this.lang.lang.search);
        this.sub = this.route.paramMap.subscribe((data: ParamMap) => {
            this.posts_open = false;
            let searchwords = data.get('search');
            let searchtype = data.get('type');
            if (searchwords && searchtype) {
                this.searchwords = searchwords.slice(0, 64);
                this.searchtype = searchtype.slice(0, 10);
                setTimeout(() => {
                    this.posts_open = true;
                }, 200);
            }
        });
    }
    sub;
    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    searchwords = "";
    searchtype = "title";
    posts_open = false;
    submit() {
        this.router.navigate(['search', {search: this.searchwords, type: this.searchtype}]);
    }
}
