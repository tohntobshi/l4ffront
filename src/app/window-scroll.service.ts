import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class WindowScrollService {

    constructor(
        @Inject(DOCUMENT) private document: any
    ) {
        this.scroll = new Subject();
        window.addEventListener('scroll', ()=> {
            if (this.blocked) {
                return;
            }
            if (this.delay) {
                return;
            }
            this.scroll.next({
                top: window.scrollY,
                bottom: window.scrollY + window.innerHeight
            });
            this.delay = true;
            setTimeout(() => {
                this.delay = false;
            }, 1000);
        });
    }
    blocked: boolean = false;
    blockScroll() {
        this.document.body.classList.add("blocked-scroll");
        this.document.getElementsByTagName('html')[0].classList.add("blocked-scroll");
        this.blocked = true;
    }
    unlockScroll() {
        this.document.body.classList.remove("blocked-scroll");
        this.document.getElementsByTagName('html')[0].classList.remove("blocked-scroll");
        this.blocked = false;
    }

    scroll: Subject<any>;
    delay = false;
}
