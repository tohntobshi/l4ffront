import { Component, OnInit, HostBinding } from '@angular/core';
import { routingAnimation } from '../animations';
import { LanguageService } from '../language.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-fav-comments',
    templateUrl: './fav-comments.component.html',
    styleUrls: ['./fav-comments.component.css'],
    animations: [ routingAnimation ]
})
export class FavCommentsComponent implements OnInit {

    constructor(
        public lang: LanguageService,
        private title: Title
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        this.title.setTitle(this.lang.lang.favorite + " - " + this.lang.lang.comments);
    }

}
