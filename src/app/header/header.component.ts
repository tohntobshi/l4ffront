import { Component } from '@angular/core';
import { SessionService } from '../session.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { RecaptchaComponent } from 'ng-recaptcha';
import { LanguageService } from '../language.service';
import { Router } from '@angular/router';
import { AuthorizationFormComponent } from '../authorization-form/authorization-form.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent {

    authorizationForm() {
        this.modalService.show(AuthorizationFormComponent, {
            class: 'modal-sm'
        });
    }
    constructor(
        public session: SessionService,
        private modalService: BsModalService,
        public lang: LanguageService,
        private router: Router
    ) { }

    navCollapse = true;

    reloadPage() {
        this.router.navigate(['e']);
        setTimeout(() => {
            this.router.navigate(['']);
        }, 200);
    }
}
