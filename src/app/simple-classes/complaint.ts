export class Complaint {
    public title: string;
    public post: number;
    public comment: number;
    public reasons: string;
    public users: string;
}
