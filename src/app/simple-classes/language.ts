export class Language {
    public upl_pic: string;
    public emb_vid: string;
    public drag_here: string;
    public paste_link: string;
    public click_to_add: string;
    public uname_or_email: string;
    public pwd: string;
    public log_in: string;
    public offence: string;
    public cp: string;
    public spam: string;
    public extremism: string;
    public flood: string;
    public other: string;
    public porn: string;
    public edit: string;
    public warn: string;
    public delete: string;
    public comm_exceed: string;
    public comment: string;
    public resp_to: string;
    public to_post: string;
    public to_begin: string;
    public next: string;
    public comp_to_comm: string;
    public comp_to_post: string;
    public from: string;
    public reason: string;
    public title_required: string;
    public title_req2: string;
    public title_exceed: string;
    public desc_exceed: string;
    public desc: string;
    public insert_tags: string;
    public divide_images: string;
    public terror: string;
    public auth_only: string;
    public post: string;
    public preview: string;
    public submit: string;
    public cancel: string;
    public authorization: string;
    public profile_banned: string;
    public activate_profile: string;
    public wrong_password: string;
    public uname_not_exist: string;
    public feed: string;
    public favorite: string;
    public posts: string;
    public comments: string;
    public subscriptions: string;
    public search: string;
    public sign_up: string;
    public complaints: string;
    public new_post: string;
    public hello: string;
    public my_profile: string;
    public messages: string;
    public log_out: string;
    public top_users: string;
    public recreate_pwd: string;
    public new_messages: string;
    public edit_title: string;
    public edit_desc: string;
    public block_tag: string;
    public search_posts: string;
    public del_tag: string;
    public add_tag: string;
    public apply: string;
    public complain: string;
    public all_comments: string;
    public except_saved: string;
    public author: string;
    public change: string;
    public subscribed: string;
    public subscribe: string;
    public ignored: string;
    public ignore: string;
    public char_left: string;
    public rating: string;
    public subscribers: string;
    public show_u_comments: string;
    public del_u_comp: string;
    public ban: string;
    public banned: string;
    public del_u_content: string;
    public pwd_changed: string;
    public display_opts: string;
    public blocked_tags: string;
    public ignored_users: string;
    public security_sets: string;
    public scrolling: string;
    public infinite: string;
    public pagination: string;
    public num_posts: string;
    public show_terror: string;
    public no_tags: string;
    public unlock_tag: string;
    public no_subs: string;
    public no_ign: string;
    public change_pwd: string;
    public pwd_required: string;
    public pwd_min_err: string;
    public pwd_max_err: string;
    public pwd_not_coinside: string;
    public active_sessions: string;
    public log_out_evr: string;
    public create_new_pwd: string;
    public success: string;
    public smt_went_wrong: string;
    public check_email: string;
    public email_not_exist: string;
    public email_required: string;
    public email_min_err: string;
    public email_invalid: string;
    public email_max_err: string;
    public by_title: string;
    public by_tags: string;
    public by_author: string;
    public username: string;
    public uname_required: string;
    public uname_min_err: string;
    public uname_invalid: string;
    public uname_max_err: string;
    public uname_exist: string;
    public email_exist: string;
    public email: string;
    public confirm_pwd: string;
    public account_verified: string;
    public tag_blocked: string;
    public tag_blocked_msg: string;
    public complaint: string;
    public complaint_msg: string;
    public delete_post: string;
    public delete_post_msg: string;
    public delete_all: string;
    public delete_all_msg: string;
    public months: string[];
    public reg_date: string;
    public old_pwd: string;
    public new_pwd: string;
    public ur_email: string;
    public more: string;
    public no_posts: string;
    public no_comments: string;
    public no_messages: string;
    public sign_up_done: string;
    public sort_rating: string;
    public sort_new_first: string;
    public sort_old_first: string;
    public sort: string;
    public remember_me: string;
    public embed_hint: string;
    public save: string;
    public l4f_desc1: string;
    public l4f_desc2: string;
    public l4f_desc3: string;
    public l4f_desc4: string;
    public reload_comp: string;
    public trad_registration: string;
    public vk: string;
    public google: string;
    public forgot_pwd: string;
    public vk_done: string;
    public vk_already_reg: string;
    public choose_nick: string;
    public google_done: string;
    public google_already_reg: string;
    public about: string;
    public sign_up_with: string
}
