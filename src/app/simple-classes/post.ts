import { Attachment } from './attachment';

export class Post {
  id: number;
  sep_id: number;
  title: string;
  description: string;
  author: string;
  date: Date;
  ratingup: number;
  ratingdown: number;
  comments: number;
  saved: number;
  currentuserrate: boolean;
  currentusersaved: boolean;
  attachments: string;
  attachments_arr: Attachment[];
  main_att: number;
  tags: string;
  hr: boolean;
  terror: boolean;
  auth_only: boolean;
  pics: number;
  vids: number;
}
