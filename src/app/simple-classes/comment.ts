import { Attachment } from './attachment';

export class Comment {
  id: number;
  sep_id: number;
  mes_ref: number;
  author: string;
  date: Date;
  post: number;
  comment: string;
  reference: number;
  ratingup: number;
  ratingdown: number;
  saved: number;
  currentuserrate: boolean;
  currentusersaved: boolean;
  isFormOpened: boolean;
  userpic: string;
  attachments_arr: Attachment[];
  warning: string;
  pics: number;
  vids: number;
}
