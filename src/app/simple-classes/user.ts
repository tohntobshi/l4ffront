export class User {
  public username: string;
  public status: string;
  public userpic: string;
  public rating: number;
  public posts: number;
  public comments: number;
  public description: string;
  public scrolltype: string;
  public numposts: number;
  public show_terror: boolean;
  public subscribed: number;
  public currentusersubscribed: boolean;
  public currentuserignored: boolean;
  public warning: string;
  public banned: boolean;
}
