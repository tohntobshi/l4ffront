import { Component, Input } from '@angular/core';
import { Comment } from '../simple-classes/comment';
import { fade } from '../animations';

@Component({
    selector: 'app-recursive-comments',
    templateUrl: './recursive-comments.component.html',
    styleUrls: ['./recursive-comments.component.css'],
    animations: [fade]
})
export class RecursiveCommentsComponent {
    @Input() comment_reference: number;
    @Input() comments: Comment[];
    constructor() { }

    trackByComments (index: number, comment: Comment): number { return comment.id; }
}
