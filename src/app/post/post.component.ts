import { Component, OnInit, OnDestroy, AfterViewInit, HostBinding } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SharedService } from '../shared.service';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { fade } from '../animations';
import { WindowScrollService } from '../window-scroll.service';
import { BackendService } from '../backend.service';
import { Post } from '../simple-classes/post';
import { Comment } from '../simple-classes/comment';
import { Title, Meta } from '@angular/platform-browser';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css'],
    animations: [ fade ]
})
export class PostComponent implements OnInit, OnDestroy, AfterViewInit {
    @HostBinding('class.fullpost') class_fullpost = true;
    @HostBinding('@fade') fade = true;
    constructor(
        private route: ActivatedRoute,
        public shared: SharedService,
        public session: SessionService,
        private router: Router,
        public lang: LanguageService,
        private scroll: WindowScrollService,
        private backend: BackendService,
        private title: Title,
        private inner: InnerApiService
    ) {}
    post: Post;
    comments: Comment[];
    commentUpdateInterval;
    parent_page_title: string;
    destroyed = false;
    ngOnInit() {
        this.comments_sort = localStorage.getItem('comm_sort') || 'rating';
        this.inner_api_sub = this.inner.api.subscribe(e => {
            switch (e.query) {
                case 'del_comment':
                    this.deleteComment(e.id);
                    break;
                case 'open_comment_form':
                    this.openCommentForm(e.id);
                    break;
                case 'post_fetch_comments':
                    this.fetchComments();
                    break;
            }
        })
        let id = this.route.snapshot.paramMap.get('id');
        let comment = this.route.snapshot.paramMap.get('comment');
        this.scroll.blockScroll();
        this.shared.fetchPost(id).subscribe(r => {
            if (this.destroyed) {
                return;
            }
            if (!r) {
                this.router.navigate(['/404']);
                return;
            }
            this.post = r.post;
            this.parent_page_title = this.title.getTitle();
            this.title.setTitle(this.post.title);
            if (r.comments) {
                this.comments = r.comments.sort((a, b) => {
                    switch (this.comments_sort) {
                        case 'rating':
                            return b.ratingup - b.ratingdown - a.ratingup + a.ratingdown;
                        case 'old_first':
                            return a.id - b.id;
                        case 'new_first':
                            return b.id - a.id;
                    }
                });
            }
            this.commentUpdateInterval = setInterval(()=>{this.fetchComments()}, 15000);
        });
        
    }
    inner_api_sub;
    isFetchCommentsProcessing = false;
    fetchComments() {
        if (this.isFetchCommentsProcessing) {
            return;
        }
        this.isFetchCommentsProcessing = true;
        let last_id = this.last_id;
        this.shared.fetchComments("postcomments", last_id, this.post.id).subscribe(r => {
            if (!r) {
                this.isFetchCommentsProcessing = false;
                return;
            }
            if (this.comments) {
                if (this.comments_sort === 'new_first') {
                    this.comments.unshift(...r);
                } else {
                    this.comments.push(...r);
                }
            } else {
                this.comments = r;
            }
            this.isFetchCommentsProcessing = false;
        });
    }
    get last_id() {
        if (!this.comments) {
            return 0;
        }
        var biggest = 0;
        this.comments.forEach(el => {
            if (biggest < el.id) {
                biggest = el.id;
            }
        });
        return biggest;
    }
    comments_sort: string;
    sortComments(sort: string) {
        localStorage.setItem('comm_sort', sort);
        this.comments_sort = sort;
        if (!this.comments) {
            return;
        }
        this.comments.sort((a, b) => {
            switch (this.comments_sort) {
                case 'rating':
                    return b.ratingup - b.ratingdown - a.ratingup + a.ratingdown;
                case 'old_first':
                    return a.id - b.id;
                case 'new_first':
                    return b.id - a.id;
            }
        });
    }


    commentsOpen = false;
    ngAfterViewInit () {
        this.commentsOpen = true;
    }
    ngOnDestroy() {
        this.inner_api_sub.unsubscribe();
        this.scroll.unlockScroll();
        this.destroyed = true;
        if (this.commentUpdateInterval) {
            clearInterval(this.commentUpdateInterval);
        }
        if (this.parent_page_title) {
            this.title.setTitle(this.parent_page_title);
        }
    }
    goBack() {
        this.router.navigate(['../../'], { relativeTo: this.route });
    }

    addTagOpen = false;
    addTag(e) {
        if (!e.result) {
            this.addTagOpen = false;
            return;
        }
        this.addTagOpen = false;
        let form = {
            query: "add_tag",
            id: this.post.id,
            tag: e.text
        }
        this.backend.query(form).subscribe(response => {
            if (!response.result) {
                return;
            }
            let tags = this.post.tags ? this.post.tags.split(',') : [];
            tags.push(e.text);
            this.post.tags = tags.join();
        })
    }
    editTitleOpen = false;
    editTitle(e) {
        if (!e.result) {
            this.editTitleOpen = false;
            return;
        }
        this.editTitleOpen = false;
        this.shared.edit(e.text, "title", this.post.id).subscribe(r => {
            if (!r) {
                return;
            }
            this.post.title = e.text;
        });
    }
    editDescriprionOpen = false;
    editDescription(e) {
        if (!e.result) {
            this.editDescriprionOpen = false;
            return;
        }
        this.editDescriprionOpen = false;
        this.shared.edit(e.text, "description", this.post.id).subscribe(r => {
            if (!r) {
                return;
            }
            this.post.description = e.text;
        });

    }
    deleteTag(tag: string) {
        let id = this.post.id;
        let form = {
            query: "delete_tag",
            id: id,
            tag: tag
        }
        this.backend.query(form).subscribe(response => {
            if (!response.result) {
                return;
            }
            if (!this.post || this.post.id != id) {
                return;
            }
            let tags = this.post.tags ? this.post.tags.split(',') : [];
            tags.splice(tags.indexOf(tag), 1);
            this.post.tags = tags.join();
        })
    }
    changePostAttributes() {
        let form = {
            query: "change_post_attributes",
            id: this.post.id,
            hr: this.post.hr,
            terror: this.post.terror,
            auth_only: this.post.auth_only
        }
        this.backend.query(form).subscribe();
    }
    deleteAttachment(src: string, type: string) {
        this.shared.deleteAttachment(src, type, this.post.id, 'post').subscribe(r => {
            if (!r) {
                return;
            }
            this.post.attachments_arr.splice(this.post.attachments_arr.findIndex(el => el.src === src && el.type === type), 1);
        })
    }
    vote(vote: boolean) {
        this.shared.vote(this.post.id, vote, 'post').subscribe(r => {
            switch (true) {
                case this.post.currentuserrate === null:
                    if (r === true) {
                        this.post.ratingup++
                    } else if (r === false) {
                        this.post.ratingdown++
                    }
                    break;
                case this.post.currentuserrate === true:
                    if (r === null) {
                        this.post.ratingup--
                    } else if (r === false) {
                        this.post.ratingup--
                        this.post.ratingdown++
                    }
                    break;
                case this.post.currentuserrate === false:
                    if (r === null) {
                        this.post.ratingdown--
                    } else if (r === true) {
                        this.post.ratingup++
                        this.post.ratingdown--
                    }
                    break;
            }
            this.post.currentuserrate = r;
            this.inner.api.next({
                query: 'vote_post',
                id: this.post.id,
                result: r
            })
        })
    }
    addToFav() {
        this.shared.addToFav(this.post.id, 'post').subscribe(r => {
            if (this.post.currentusersaved && !r) {
                this.post.saved--;
            } else if (!this.post.currentusersaved && r) {
                this.post.saved++;
            }
            this.post.currentusersaved = r;
            this.inner.api.next({
                query: 'fav_post',
                id: this.post.id,
                result: r
            })
        })
    }
    isFormOpened: boolean = false;
    openCommentForm(id: number) {
        if (id === 0) {
            if (this.isFormOpened) {
                this.isFormOpened = false;
            } else {
                if (this.comments) {
                    this.comments.forEach(el => {
                        el.isFormOpened = false;
                    });
                }
                this.isFormOpened = true;
            }
            return;
        }
        this.comments.forEach(el => {
            if(el.id === id && !el.isFormOpened) {
                this.isFormOpened = false;
                el.isFormOpened = true;
            } else {
                el.isFormOpened = false;
            }
        })
    }
    deleteComment(id: number) {
        let form = {
            query: "delete_comment",
            id: id
        }
        this.backend.query(form).subscribe(r => {
            if (!r.result) {
                return;
            }
            this.comments.splice(this.comments.findIndex(el => el.id === id), 1);
        })
    }
}
