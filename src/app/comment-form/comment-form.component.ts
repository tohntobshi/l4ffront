import { Component, OnDestroy, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AttachmentService } from '../attachment-form/attachment.service';
import { LanguageService } from '../language.service';
import { BackendService } from '../backend.service';
import { fade } from '../animations';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-comment-form',
    templateUrl: './comment-form.component.html',
    styleUrls: ['./comment-form.component.css'],
    providers: [AttachmentService],
    animations: [ fade ]
})
export class CommentFormComponent implements OnDestroy, OnInit {
    @Output() onComment = new EventEmitter();
    @Input() reference: number;
    @Input() post_id: number;
    constructor(
        public attSrv: AttachmentService,
        public lang: LanguageService,
        private backend: BackendService,
        private inner: InnerApiService
    ) { }
    ngOnInit() {
        this.inner_api_sub = this.inner.api.subscribe(e => {
            if (e.query === 'drag') {
                this.attachment_form = true;
            }
        })
    }
    inner_api_sub;
    ngOnDestroy() {
        this.inner_api_sub.unsubscribe();
        this.attSrv.clearAttachments();
    }
    form = new FormGroup({
        comment: new FormControl('', [
            Validators.required,
            Validators.maxLength(5000)
        ])
    });
    get comment() { return this.form.get('comment') }
    submit() {
        let attachments_to_send;
        if (this.attSrv.attachments) {
            this.attSrv.attachments.forEach((attachment) => {
                let attachment_to_send = {
                    src: attachment.src,
                    type: attachment.type
                }
                if(attachments_to_send) {
                    attachments_to_send.push(attachment_to_send);
                } else {
                    attachments_to_send = [attachment_to_send];
                }
            });
        }
        var recipients = this.form.controls.comment.value.match(/(^| |,)@[a-zA-Z0-9а-яА-ЯёЁ_\-]{4,}/gm);
        if(recipients != null) {
            recipients.forEach((recipient, index, recipients) => {
                recipients[index] = recipient.split('@')[1];
            });
            recipients = new Set(recipients);
            recipients = Array.from(recipients);
        }
        let form = {
            query: "create_comment",
            comment: this.form.controls.comment.value,
            post_id: this.post_id,
            reference: this.reference,
            attachments: attachments_to_send,
            recipients: recipients
        }
        this.backend.query(form).subscribe(r => {
            if (!r.result) {
                return;
            }
            this.form.reset();
            this.onComment.emit();
            this.inner.api.next({
                query: "post_fetch_comments"
            })
        });
    }
    attachment_form: boolean = false;

    onDragStart(e, index:number) {
        e.dataTransfer.dropEffect = "move";
        e.dataTransfer.setData("index", index);
    }
    onDrop(e, index:number) {
        e.preventDefault();
        let att_index = e.dataTransfer.getData("index");
        let target_index = index;
        this.attSrv.swapAttachments(att_index, target_index);
    }
    onDragOver(e) {
        e.preventDefault();
    }
}
