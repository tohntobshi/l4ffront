import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../shared.service';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { LanguageService } from '../language.service';

@Component({
  selector: 'app-trad-registration',
  templateUrl: './trad-registration.component.html',
  styleUrls: ['./trad-registration.component.css'],
})
export class TradRegistrationComponent implements OnInit {

  constructor(
      private backend: BackendService,
      private router: Router,
      public shared: SharedService,
      public lang: LanguageService
  ) { }
  form: FormGroup;
  done = false;
  ngOnInit() {
      this.form = new FormGroup({
          'username': new FormControl('', [
              Validators.required,
              Validators.minLength(4),
              Validators.pattern(/^[a-zA-Z0-9а-яА-ЯёЁ_\-]*$/),
              Validators.maxLength(32)
          ], this.validateUsername.bind(this)),
          'email': new FormControl('', [
              Validators.required,
              Validators.minLength(6),
              Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
              Validators.maxLength(64)
          ], this.validateEmail.bind(this)),
          'password': new FormControl('', [
              Validators.required,
              Validators.minLength(6),
              Validators.maxLength(32)
          ]),
          'password2': new FormControl('', Validators.required),
      });
  }
  comparePasswords() {
      if (!this.form) {
          return false;
      }
      return this.password.value == this.password2.value ? false : true;
  }
  validateUsername(control: AbstractControl) {
      return Observable.timer(2000).switchMap(() => {
          let form = {
              query: "check_username",
              username: control.value
          }
          return this.backend.query(form).map(res => {
              return res.result ? null : { usernameTaken: true };
          });
      });
  }
  validateEmail(control: AbstractControl) {
      return Observable.timer(2000).switchMap(() => {
          let form = {
              query: "check_email",
              email: control.value
          }
          return this.backend.query(form).map(res => {
              return res.result ? null : { emailTaken: true };
          });
      });
  }
  get username() { return this.form.get('username') }
  get email() { return this.form.get('email') }
  get password() { return this.form.get('password') }
  get password2() { return this.form.get('password2') }
  @ViewChild('captchaRef') captchaRef;
  submit(captcha: string) {
      let form = {
          query: 'sign_up',
          username: this.username.value,
          email: this.email.value,
          password: this.password.value,
          captcha: captcha
      }
      this.backend.query(form).subscribe(response => {
          this.captchaRef.reset();
          if (response.result) {
              this.done = true;
              this.form.reset();
              setTimeout(() => {
                  this.router.navigate(['feed']);
              }, 10000);
          }
      })
  }

}
