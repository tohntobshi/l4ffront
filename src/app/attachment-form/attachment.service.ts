import { Injectable } from '@angular/core';
import { Attachment } from '../simple-classes/attachment';

@Injectable()
export class AttachmentService {
    constructor() {
    }
    main_att: number;
    attachments: Attachment[];
    addAttachment(type, src, embed_src = undefined) {
        if (src.length > 128 || type.length > 10) {
            return;
        }
        let attachment = {
            type: type,
            src: src,
            embed_src: embed_src
        }
        if(this.attachments) {
            this.attachments.push(attachment);
        } else {
            this.attachments = [attachment];
        }
    }

    sortUp(i) {
        if (i == 0) {
            return;
        }
        let tmp = this.attachments[i-1];
        this.attachments[i-1] = this.attachments[i];
        this.attachments[i] = tmp;
    }
    sortDown(i) {
        if (i == this.attachments.length - 1) {
            return;
        }
        let tmp = this.attachments[i+1];
        this.attachments[i+1] = this.attachments[i];
        this.attachments[i] = tmp;
    }
    swapAttachments(att_index: number, target_index: number) {
        let tmp = this.attachments[target_index];
        this.attachments[target_index] = this.attachments[att_index];
        this.attachments[att_index] = tmp;
    }
    delete(i) {
        this.attachments.splice(i, 1);
        this.main_att = undefined;
    }
    clearAttachments() {
        let attachments: Attachment[];
        this.attachments = attachments;
    }
}
