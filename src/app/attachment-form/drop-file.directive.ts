import { Directive, HostListener, ElementRef, EventEmitter, Output, HostBinding } from '@angular/core';

@Directive({
  selector: '[dropFiles]'
})
export class DropFileDirective
{
  @Output() dropFiles = new EventEmitter();

  constructor(private el: ElementRef) {}

  @HostBinding('class.dragged') dragged: boolean;

  @HostListener('drop', ['$event'])
  onDrop(event) {
    event.preventDefault();
    event.stopPropagation();
    let files = event.dataTransfer.files;
    this.dropFiles.emit(files);
    this.dragged = false;
  }
  @HostListener('dragover', ['$event'])
  onDragover(event) {
    event.preventDefault();
    this.dragged = true;
  }
  @HostListener('dragleave')
  onDragleave() {
    this.dragged = false;
  }
}
