import { Component, Input, OnInit, OnDestroy, ElementRef, AfterViewInit } from '@angular/core';
import { Attachment } from '../simple-classes/attachment';
import { BackendService } from '../backend.service';
import { AttachmentService } from '../attachment-form/attachment.service';
import { LanguageService } from '../language.service';
import { InnerApiService } from '../inner-api.service';
import { SharedService } from '../shared.service';


@Component({
    selector: 'app-attachment-form',
    templateUrl: './attachment-form.component.html',
    styleUrls: ['./attachment-form.component.css']
})
export class AttachmentFormComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() autoOpen: boolean = false;
    @Input() maxAtts: number = 10;
    constructor(
        private backend: BackendService,
        public attSrv: AttachmentService,
        public lang: LanguageService,
        private el: ElementRef,
        private inner: InnerApiService,
        private shared: SharedService
    ) { }

    ngOnInit() {
        if (this.autoOpen) {
            this.isUplPicCollapsed = false;
        }
        this.inner_api_sub = this.inner.api.subscribe((e) => {
            if (e.query === "drag") {
                this.isEmbedCollapsed = true;
                this.isUplPicCollapsed = false;
                this.el.nativeElement.scrollIntoView();
            }
        });

    }
    ngAfterViewInit() {
        if (this.autoOpen) {
            this.el.nativeElement.scrollIntoView();
        }
    }
    inner_api_sub;
    ngOnDestroy() {
        this.inner_api_sub.unsubscribe();
    }
    isBusy = false;
    isEmbedCollapsed = true;
    isUplPicCollapsed = true;
    collapse(x) {
        if(x=="vid") {
            if (this.isEmbedCollapsed) {
                this.isUplPicCollapsed = true;
                this.isEmbedCollapsed = false;
            } else {
                this.isEmbedCollapsed = true;
            }
        }
        if(x=="pic") {
            if (this.isUplPicCollapsed) {
                this.isEmbedCollapsed = true;
                this.isUplPicCollapsed = false;
            } else {
                this.isUplPicCollapsed = true;
            }
        }
    }

    fileDrop(e) {
        this.send(e);
    }
    fileOpen(file) {
        this.send(file.files)
    }
    send(files) {
        this.isBusy = true;
        let form = {
            query: "upload_images"
        };
        if (!files) {
            this.isBusy = false;
            return;
        }
        let sum_size = 0;
        for (var i = 0; i < files.length; i++) {
            sum_size = sum_size + files[i].size;
            if (!["image/jpeg", "image/gif", "image/png"].includes(files[i].type) || files[i].size > 10485760) {
                this.isBusy = false;
                return;
            }
        }
        if (sum_size > 10485760) {
            this.isBusy = false;
            return;
        }
        let max_files = this.maxAtts - (this.attSrv.attachments ? this.attSrv.attachments.length : 0);
        this.backend.query(form, files, max_files).subscribe(response => {
            if (!response.result) {
                this.isBusy = false;
                return;
            }
            response.data.attachments.forEach((el) => {
                this.attSrv.addAttachment(el.type, el.src);
            });
            this.isBusy = false;
        });
    }

    embed_link: string;
    checkEmbed() {
        if (!this.embed_link) {
            return;
        }
        let raw_link = this.embed_link;
        this.embed_link = '';
        let link = raw_link.split('/');
        if (!link[2]) {
            return;
        }
        let src;
        let type;
        let embed_src;
        if (link[2].slice(-11) === 'youtube.com') {
            let details = link[3].match(/v=[a-zA-Z0-9_\-]+/);
            if(details) {
                src = details[0].slice(2);
                type = 'youtube';
            }
        }
        if (link[2] === 'vimeo.com') {
            let check = /^[0-9]*$/;
            if (check.test(link[link.length - 1])) {
                src = link[link.length - 1];
                type = 'vimeo';
            }
        }
        if (link[2] === 'coub.com') {
            let check = /^[a-z0-9]*$/;
            if (check.test(link[4])) {
                src = link[4];
                type = 'coub';
            }
        }
        // if (link[2] === 'www.redtube.com') {
        //     let check = /^[0-9]*$/;
        //     if (check.test(link[3])) {
        //         src = link[3];
        //         type = 'redtube';
        //     }
        // }
        // if (link[2].slice(-11) === 'pornhub.com') {
        //     let details = link[3].match(/viewkey=[a-z0-9]+/);
        //     if(details) {
        //         src = details[0].slice(8);
        //         type = 'pornhub';
        //     }
        // }
        // if (link[2].slice(-11) === 'youporn.com') {
        //     let details = raw_link.match(/watch\/[0-9]+/);
        //     if(details) {
        //         src = details[0].slice(6);
        //         type = 'youporn';
        //     }
        // }
        // if (link[2] === 'www.tube8.com') {
        //     if (link[3] && link[4] && link[5] && link[3]!='cat' && /^[a-z]*$/.test(link[3]) && /^[a-z\-0-9_\.]*$/.test(link[4]) && /^[0-9]*$/.test(link[5])) {
        //         src = link[3] + '/' + link[4] + '/' + link[5];
        //         type = 'tube8';
        //     }
        // }
        // if (link[2] === 'www.xtube.com') {
        //     if (link[3] && link[4] && link[3] === "video-watch" && /^[a-z\-0-9]+$/.test(link[4])) {
        //         src = link[4];
        //         type = 'xtube';
        //     }
        // }
        // if (link[2] === 'www.spankwire.com') {
        //     let details = raw_link.match(/video[0-9]+/);
        //     if(details) {
        //         src = details[0].slice(5);
        //         type = 'spankwire';
        //     }
        // }
        // if (link[2] === 'www.keezmovies.com') {
        //     let details = raw_link.match(/video\/[0-9a-z\-]+/);
        //     if(details) {
        //         src = details[0].slice(6);
        //         type = 'keezmovies';
        //     }
        // }
        if (link[2] === 'gfycat.com') {
            let details = raw_link.match(/\/gifs\/detail\/[a-zA-Z]+$/);
            if (details) {
                src = details[0].split('/')[3];
                type = 'gfycat';
            }
        }
        if (src && type) {
            embed_src = this.shared.getSrc({
                src: src,
                type: type
            });
            this.attSrv.addAttachment(type, src, embed_src);
        }
    }

}
