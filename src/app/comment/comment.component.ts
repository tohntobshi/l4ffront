import { Component, TemplateRef, ViewChild, Input, ElementRef, AfterViewInit } from '@angular/core';
import { Comment } from '../simple-classes/comment';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { fade } from '../animations';
import { SharedService } from '../shared.service';
import { ActivatedRoute } from '@angular/router';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.css'],
    animations: [fade]
})
export class CommentComponent implements AfterViewInit {
    @Input() separate: boolean = false;
    @Input() comment: Comment;

    constructor(
        public session: SessionService,
        public lang: LanguageService,
        private shared: SharedService,
        private route: ActivatedRoute,
        private el: ElementRef,
        private inner: InnerApiService
    ) { }

    get upic() {
        if(this.comment.userpic) {
            return "/userpics/" + this.comment.userpic + ".jpg";
        } else { return "/userpics/default.jpg"; }
    }

    ngAfterViewInit() {
        let comment = this.route.snapshot.paramMap.get('comment');
        if (this.comment.id === +comment) {
          setTimeout(() => {
            this.el.nativeElement.scrollIntoView();
          }, 1000);
        }
    }
    commentForm: boolean = false;
    vote(vote: boolean) {
        this.shared.vote(this.comment.id, vote, 'comment').subscribe(r => {
            switch (true) {
                case this.comment.currentuserrate === null:
                    if (r === true) {
                        this.comment.ratingup++;
                    } else if (r === false) {
                        this.comment.ratingdown++;
                    }
                    break;
                case this.comment.currentuserrate === true:
                    if (r === null) {
                        this.comment.ratingup--;
                    } else if (r === false) {
                        this.comment.ratingup--;
                        this.comment.ratingdown++;
                    }
                    break;
                case this.comment.currentuserrate === false:
                    if (r === null) {
                        this.comment.ratingdown--;
                    } else if (r === true) {
                        this.comment.ratingup++;
                        this.comment.ratingdown--;
                    }
                    break;
            }
            this.comment.currentuserrate = r;
        })
    }
    addToFav() {
        this.shared.addToFav(this.comment.id, 'comment').subscribe(r => {
            if (this.comment.currentusersaved && !r) {
                this.comment.saved--;
            } else if (!this.comment.currentusersaved && r) {
                this.comment.saved++;
            }
            this.comment.currentusersaved = r;
        })
    }
    openCommentForm() {
        this.inner.api.next({
            query: 'open_comment_form',
            id: this.comment.id
        })
    }
    editCommentOpen = false;
    editComment(e) {
        if (!e.result) {
            this.editCommentOpen = false;
          return;
        }
        this.editCommentOpen = false;
        this.shared.edit(e.text, "comment", this.comment.id).subscribe(r => {
            if (!r) {
                return;
            }
            this.comment.comment = e.text;
        })
    }
    warnCommentOpen = false;
    warnComment(e) {
        if (!e.result) {
            this.warnCommentOpen = false;
          return;
        }
        this.warnCommentOpen = false;
        this.shared.warnComment(e.text, this.comment.id).subscribe(r => {
            if (!r) {
                return;
            }
            this.comment.warning = e.text;
        })
    }
    deleteAttachment(src: string, type: string) {
        this.shared.deleteAttachment(src, type, this.comment.id, 'comment').subscribe(r => {
            if (!r) {
                return;
            }
            this.comment.attachments_arr.splice(this.comment.attachments_arr.findIndex(el => el.src === src && el.type === type), 1);
        })
    }
    deleteComment() {
        this.inner.api.next({
            query: 'del_comment',
            id: this.comment.id
        })
    }
}
