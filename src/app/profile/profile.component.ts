import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { SessionService } from '../session.service';
import { User } from '../simple-classes/user';
import { LanguageService } from '../language.service';
import { routingAnimation, slideFromTop } from '../animations';
import { SharedService } from '../shared.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css'],
    animations: [ routingAnimation, slideFromTop ]
})
export class ProfileComponent implements OnInit, OnDestroy {
    contentOpen = false;
    @HostBinding('@fadeInOut') fadeInOut = true;
    constructor(
        private route: ActivatedRoute,
        public session: SessionService,
        private backend: BackendService,
        private router: Router,
        public lang: LanguageService,
        public shared: SharedService,
        private title: Title
    ) { }

    get upic() {
        if(this.user.userpic) {
            return "/userpics/" + this.user.userpic + ".jpg";
        } else { return "/userpics/default.jpg"; }
    }

    user: User;
    getUser(username: string) {
        this.title.setTitle(username);
        if (this.route.children.length > 0) {
            return;
        }
        this.contentOpen = false;
        let form = {
            query: "user",
            username: username
        };
        this.backend.query(form).subscribe(response => {
            if (this.destroyed) {
                return;
            }
            if (!response.result) {
                this.router.navigate(['/404']);
                return;
            }
            this.user = response.data.user;
            this.contentOpen = true;
        });
    }


    ngOnInit() {
        this.sub = this.route.paramMap.subscribe((data: ParamMap) => {
            this.getUser(data.get('user'));
            this.upic_options = false;
            this.isEditOpen = false;
        });
    }
    sub;
    destroyed = false;
    ngOnDestroy() {
        this.sub.unsubscribe();
        this.destroyed = true;
    }

    upic_options = false;
    isEditOpen = false;
    description: string;
    user_comments = false;

    showUserComments() {
        if (!this.user_comments) {
            this.user_comments = true;
        } else {
            this.user_comments = false;
        }
    }

    editDescription() {
        this.description = this.user.description ? this.user.description : "";
        this.isEditOpen = true;
    }
    changeDescription() {
        this.isEditOpen = false;
        this.user.description = this.description;
        let form = {
            query: "change_description",
            description: this.description
        }
        this.backend.query(form).subscribe();
    }
    changeUserpic(files) {
        if (this.session.session.username != this.user.username) {
            return;
        }
        if (!["image/jpeg", "image/gif", "image/png"].includes(files[0].type) || files[0].size > 5242880) {
            return;
        }
        let form = {
            query: "change_userpic"
        };
        this.backend.query(form, files, 1).subscribe(response => {
            if (!response.result) {
                return;
            }
            if (!files) {
                return;
            }
            this.session.session.userpic = response.data.userpic;
            if (this.session.session.username == this.user.username) {
                this.user.userpic = response.data.userpic;
            }
        });
    }
    onClickChangeButton(e) {
        e.stopPropagation();
    }
    deleteUserpic(e) {
        e.stopPropagation();
        let form = {
            query: "delete_userpic"
        }
        this.backend.query(form).subscribe(response => {
            this.session.session.userpic = null;
            if (this.session.session.username == this.user.username) {
                this.user.userpic = null;
            }
        })
    }
    ignore() {
        this.shared.ignore(this.user.username).subscribe(r => {
            this.user.currentuserignored = r;
        })
    }
    subscribe() {
        this.shared.subscribe(this.user.username).subscribe(r => {
            this.user.currentusersubscribed = r;
        })
    }
    warn_user = false;
    warnUser(e) {
        if (!e.result) {
            this.warn_user = false;
            return;
        }
        let form = {
            query: "warn_user",
            username: this.user.username,
            warning: e.text
        }
        this.warn_user = false;
        this.backend.query(form).subscribe(r => {
            if (r.result) {
                this.user.warning = e.text;
            }
        });
    }
    banUser() {
        let form = {
            query: "ban",
            username: this.user.username
        }
        this.backend.query(form).subscribe(r => {
            if (r.result) {
                this.user.banned = true;
            }
        });
    }
    unbanUser() {
        let form = {
            query: "unban",
            username: this.user.username
        }
        this.backend.query(form).subscribe(r => {
            if (r.result) {
                this.user.banned = false;
            }
        })
    }
    deleteUserComplaints() {
        let form = {
            query: "delete_user_complaints",
            username: this.user.username
        }
        this.backend.query(form).subscribe();
    }
}
