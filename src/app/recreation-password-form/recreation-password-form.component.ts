import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BackendService } from '../backend.service';
import { LanguageService } from '../language.service';
import { SharedService } from '../shared.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-recreation-password-form',
    templateUrl: './recreation-password-form.component.html',
    styleUrls: ['./recreation-password-form.component.css']
})
export class RecreationPasswordFormComponent implements OnInit {

    constructor(
        private backend: BackendService,
        public lang: LanguageService,
        public shared: SharedService,
        private modalRef: BsModalRef
    ) { }

    ngOnInit() {
        this.form = new FormGroup({
                'email': new FormControl('', [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
                    Validators.maxLength(64)
                ])
        });
    }
    @ViewChild('captchaRef') captchaRef;
    form: FormGroup;
    get email() { return this.form.get('email') }
    submit(captcha: string) {
        let form = {
            query: "recreate_password_init",
            email: this.email.value,
            captcha: captcha
        }
        this.backend.query(form).subscribe(response => {
            this.captchaRef.reset();
            if (response.result) {
                this.alert = {
                    type: "success",
                    msg: this.lang.lang.check_email
                }
            } else {
                this.alert = {
                    type: "danger",
                    msg: this.lang.lang.email_not_exist
                }
            }
            setTimeout(() => {
                this.alert = undefined;
                if (response.result) {
                    this.modalRef.hide();
                }
            }, 5000)
        })
    }
    alert;
}
