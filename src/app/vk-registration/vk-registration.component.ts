import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../language.service';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';

declare var VK:any;

@Component({
    selector: 'app-vk-registration',
    templateUrl: './vk-registration.component.html',
    styleUrls: ['./vk-registration.component.css']
})

export class VkRegistrationComponent implements OnInit {

    constructor(
        public lang: LanguageService,
        private backend: BackendService,
        private router: Router
    ) {}
    done = false;
    already_registered = false;
    form: FormGroup;
    ngOnInit() {
        this.form = new FormGroup({
            'username': new FormControl('', [
                Validators.required,
                Validators.minLength(4),
                Validators.pattern(/^[a-zA-Z0-9а-яА-ЯёЁ_\-]*$/),
                Validators.maxLength(32)
            ], this.validateUsername.bind(this))
        });
    }
    get username() { return this.form.get('username') }
    submit() {
        VK.Auth.login(response => {
            if (!response || response.status != 'connected') {
                return;
            }
            let form = {
                query: "sign_up_vk",
                username: this.username.value,
                expire: response.session.expire,
                mid: response.session.mid,
                secret: response.session.secret,
                sid: response.session.sid,
                sig: response.session.sig,
            }
            this.backend.query(form).subscribe(r => {
                if (!r.result) {
                    if(r.error && r.error === "already_registered") {
                        this.done = true;
                        this.already_registered = true;
                        setTimeout(() => {
                            this.router.navigate(['']);
                        }, 5000);
                    }
                    return;
                }
                this.done = true;
                setTimeout(() => {
                    this.router.navigate(['']);
                }, 5000);
            });
        })
    }
    validateUsername(control: AbstractControl) {
        return Observable.timer(2000).switchMap(() => {
            let form = {
                query: "check_username",
                username: control.value
            }
            return this.backend.query(form).map(res => {
                return res.result ? null : { usernameTaken: true };
            });
        });
    }
}
