import { Component, OnDestroy, Input, ElementRef, ViewChild, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { postAnimation, slideFromTop } from '../animations';
import { WindowScrollService } from '../window-scroll.service';
import { Comment } from '../simple-classes/comment';
import { BackendService } from '../backend.service';
import { ActivatedRoute } from '@angular/router';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.css'],
    animations: [postAnimation, slideFromTop]
})

export class CommentsComponent implements OnDestroy, OnInit {
    @Input() ifEmpty: string;
    @Input() commentsType: string;
    @Input() userName: string;

    @ViewChild('commentsWrapper') commentsWrapper: ElementRef;
    constructor(
        public shared: SharedService,
        public session: SessionService,
        public lang: LanguageService,
        private scroll: WindowScrollService,
        private backend: BackendService,
        private route: ActivatedRoute,
        private inner: InnerApiService
    ) { }
    trackByComments (index: number, comment: Comment): number { return comment.id; }
    comments: Comment[];
    ngOnInit() {
        this.inner_api_sub = this.inner.api.subscribe(e => {
            switch(e.query) {
                case 'comments_start_page':
                    this.startPage();
                    break;
            }
        });
        this.startPage();
        this.scroll_sub = this.scroll.scroll.subscribe(s => {
            if (this.session.session && this.session.session.scrolltype === 'pagination') {
                return;
            }
            if (this.commentsWrapper.nativeElement.clientHeight < s.bottom + 500) {
                this.fetchComments(false, false);
            }
        });
    }
    inner_api_sub;
    scroll_sub;
    ngOnDestroy() {
        if (this.scroll_sub) {
            this.scroll_sub.unsubscribe();
        }
        this.inner_api_sub.unsubscribe();
    }
    index(id: number) {
        return this.comments.findIndex(el => el.id === id);
    }
    startPage() {
        this.fetchComments(true, true);
    }
    nextPage() {
        this.fetchComments(true, false);
    }
    isFetchCommentsProcessing = false;
    fetchComments(new_page: boolean, reset_id: boolean) {
        if (this.isFetchCommentsProcessing) {
            return;
        }
        if (this.route.children.length > 0) {
            return;
        }
        this.isFetchCommentsProcessing = true;
        let last_id = reset_id ? 0 : this.last_id;
        if (new_page) {
            let comments: Comment[];
            this.comments = comments;
        }
        this.shared.fetchComments(this.commentsType, last_id, 0, this.userName).subscribe(r => {
            if (!r) {
                this.isFetchCommentsProcessing = false;
                return;
            }
            if (new_page) {
                window.scrollTo(0, 0);
                this.comments = r;
            } else {
                this.comments.push(...r);
            }
            this.isFetchCommentsProcessing = false;
        });
    }
    get last_id() {
        if (!this.comments) {
            return 0;
        }
        var smallest: number;
        this.comments.forEach(comment => {
            if (comment.sep_id) {
                if((smallest && smallest > comment.sep_id) || !smallest) {
                    smallest = comment.sep_id;
                }
            }
        });
        return smallest || 0;
    }
    deleteMessage(sep_id) {
        this.comments.splice(this.comments.findIndex(el => el.sep_id === sep_id), 1);
        let isMessagesLeft = false;
        this.comments.forEach(element => {
            if (element.sep_id != null) {
                isMessagesLeft = true;
            }
        });
        if (!isMessagesLeft){
            let comments: Comment[];
            this.comments = comments;
        }
        let form = {
            query: "delete_message",
            message: sep_id
        }
        this.backend.query(form).subscribe();
    }
}
