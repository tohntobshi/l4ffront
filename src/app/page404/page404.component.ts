import { Component, OnInit, HostBinding } from '@angular/core';
import { routingAnimation, slideSeparately } from '../animations';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-page404',
    templateUrl: './page404.component.html',
    styleUrls: ['./page404.component.css'],
    animations: [ routingAnimation, slideSeparately ]
})
export class Page404Component implements OnInit {
    @HostBinding('@fadeInOut') fadeInOut = true;
    constructor(
        private title: Title
    ) { }

    ngOnInit() {
        this.title.setTitle("404");
    }

}
