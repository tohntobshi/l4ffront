import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { Post } from './simple-classes/post';
import { Comment } from './simple-classes/comment';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NotificationComponent } from './notification/notification.component';
import { AboutComponent } from './about/about.component';
import 'rxjs/add/operator/map';
import { LanguageService } from './language.service';

@Injectable()
export class SharedService {
    constructor(
        private modalService: BsModalService,
        private backend: BackendService,
        private sanitizer: DomSanitizer,
        private lang: LanguageService,
    ) {}
    // recaptchasitekey = "6Lc5eTwUAAAAAP9I2cqqgAs_hpSNsueKFLgQPw70"; // l4f
    recaptchasitekey = "6LfytTYUAAAAANovhLIQBD936qVidUOdvhkchjBG"; // localhost, demo
    google_client_id = "660570001388-2k2i55q2lcjjsdfa3eq5d1np8kttbta5.apps.googleusercontent.com"; // localhost, demo
    // google_client_id = "611183975564-7dajhukq218i06610r9rrlit4in9m0o5.apps.googleusercontent.com"; // l4f
    fetchPosts(type: string, last_id: number, searchtype: string = null, searchwords: string = null) {
        let form = {
            query: type,
            last_id: last_id,
            searchwords: searchwords,
            searchtype: searchtype
        }
        return this.backend.query(form).map(r => {
            if (!r.result) {
                return false;
            }
            let fetchedPosts: Post[] = r.data.posts;
            fetchedPosts.forEach(this.getAttArr, this);
            return fetchedPosts;
        })
    }

    fetchPost(id) {
        let form = {
            query: "post",
            post_id: id
        }
        return this.backend.query(form).map(r => {
            if (!r.result) {
                return false;
            }
            let post: Post = r.data.post;
            this.getAttArr(post);
            let comments: Comment[] = r.data.comments;
            if (comments) {
                comments.forEach(this.getAttArr, this);
            }
            return {
                post: post,
                comments: comments
            }
        })
    }

    fetchComments(type: string, last_id: number, post_id: number, username: string = null) {
        let form = {
            query: type,
            post_id: post_id,
            last_id: last_id,
            username: username
        }
        return this.backend.query(form).map(r => {
            if(!r.result) {
                return false;
            }
            let comments: Comment[] = r.data.comments;
            comments.forEach(this.getAttArr, this);
            return comments;
        })
    }


    getSrc(attachment) {
        switch(attachment.type) {
            case 'youtube': return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + attachment.src);
            case 'coub': return this.sanitizer.bypassSecurityTrustResourceUrl("//coub.com/embed/" + attachment.src);
            case 'vimeo': return this.sanitizer.bypassSecurityTrustResourceUrl("https://player.vimeo.com/video/" + attachment.src);
            case 'redtube': return this.sanitizer.bypassSecurityTrustResourceUrl("https://embed.redtube.com/?id=" + attachment.src);
            case 'pornhub': return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.pornhub.com/embed/" + attachment.src);
            case 'youporn': return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youporn.com/embed/" + attachment.src);
            case 'tube8': return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.tube8.com/embed/" + attachment.src);
            case 'xtube': return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.xtube.com/video-watch/embedded/" + attachment.src);
            case 'spankwire': return this.sanitizer.bypassSecurityTrustResourceUrl("//www.spankwire.com/EmbedPlayer.aspx?ArticleId=" + attachment.src);
            case 'keezmovies': return this.sanitizer.bypassSecurityTrustResourceUrl("//www.keezmovies.com/embed/" + attachment.src);
            case 'gfycat': return this.sanitizer.bypassSecurityTrustResourceUrl("https://gfycat.com/ifr/" + attachment.src + "?autoplay=0");
        }
    }
    getAttArr(post) {
        post.pics = 0;
        post.vids = 0;
        if (post.attachments) {
            let attachments = post.attachments.split(',');
            attachments.forEach((attachment) => {
                let att = {
                    src: attachment.split(':')[0],
                    type: attachment.split(':')[1],
                    embed_src: undefined
                };
                if (att.type === "jpg" || att.type === "gif") {
                    post.pics++;
                } else {
                    post.vids++;
                }
                att.embed_src = this.getSrc(att);
                if(post.attachments_arr) {
                    post.attachments_arr.push(att);
                } else {
                    post.attachments_arr = [att];
                }
            });
        }
    }

    blockTag(tag: string) {
        let form = {
            query: "block_tag",
            tag: tag
        }
        this.backend.query(form).subscribe(response => {
            if(response.result) {
                let modalRef = this.modalService.show(NotificationComponent);
                modalRef.content.title = this.lang.lang.tag_blocked;
                modalRef.content.message = this.lang.lang.tag_blocked_msg;
            }
        })
    }
    complain(id: number, type: string, reason: string) {
        let form = {
            query: "complain",
            id: id,
            type: type,
            reason: reason
        }
        this.backend.query(form).subscribe(response => {
            if(response.result) {
                let modalRef = this.modalService.show(NotificationComponent);
                modalRef.content.title = this.lang.lang.complaint;
                modalRef.content.message = this.lang.lang.complaint_msg;
            }
        })
    }

    vote(id: number, vote: boolean, type: string) {
        let form = {
            query: "vote",
            id: id,
            vote: vote,
            type: type
        }
        return this.backend.query(form).map(r => r.result);
    }
    addToFav(id: number, type: string) {
        let form = {
            query: "favorite",
            id: id,
            type: type
        }
        return this.backend.query(form).map(r => r.result);
    }


    ignore(username) {
        let form = {
            query: "ignore",
            username: username
        }
        return this.backend.query(form).map(r => r.result);
    }
    subscribe(username) {
        let form = {
            query: "subscribe",
            username: username
        }
        return this.backend.query(form).map(r => r.result);
    }

    deleteAttachment(src: string, type: string, id: number, content_type: string) {
        let form = {
            query: "delete_attachment",
            src: src,
            type: type,
            id: id,
            content_type: content_type
        }
        return this.backend.query(form).map(r => r.result)
    }
    deletePost(id: number, all_comments: boolean) {
        let form = {
            query: "delete_post",
            id: id,
            all_comments: all_comments
        }
        this.backend.query(form).subscribe(response => {
            if (!response.result) {
                return;
            }
            let modalRef = this.modalService.show(NotificationComponent);
            modalRef.content.title = this.lang.lang.delete_post;
            modalRef.content.message = this.lang.lang.delete_post_msg;
        });
    }
    edit(text: string, type: string, id: number) {
        let form = {
            query: "edit",
            text: text,
            type: type,
            id: id
        }
        return this.backend.query(form).map(r => r.result);
    }
    warnComment(warning: string, id: number) {
        let form = {
            query: "warn_comment",
            id: id,
            warning: warning
        }
        return this.backend.query(form).map(r => r.result)
    }
    deleteAll(time, username) {
        if (!time){
            return;
        }
        let datetime = new Date(time);
        let year = datetime.getUTCFullYear();
        let month = (datetime.getUTCMonth() + 1).toString().length === 1 ? "0"+(datetime.getUTCMonth() + 1) : (datetime.getUTCMonth() + 1);
        let date = datetime.getUTCDate().toString().length === 1 ? "0"+datetime.getUTCDate() : datetime.getUTCDate();
        let hours = datetime.getUTCHours().toString().length === 1 ? "0"+datetime.getUTCHours() : datetime.getUTCHours();
        let minutes = datetime.getUTCMinutes().toString().length === 1 ? "0"+datetime.getUTCMinutes() : datetime.getUTCMinutes();
        let seconds = "00";
        let datestring = year+"-"+month+"-"+date+" "+hours+":"+minutes+":"+seconds;
        let form = {
            query: "delete_all",
            username: username,
            date: datestring
        }
        this.backend.query(form).subscribe(response => {
            if (!response.result) {
                return;
            }
            let modalRef = this.modalService.show(NotificationComponent);
            modalRef.content.title = this.lang.lang.delete_all;
            modalRef.content.message = this.lang.lang.delete_all_msg;
        });
    }
    showSiteInfo() {
        this.modalService.show(AboutComponent);
    }
}
