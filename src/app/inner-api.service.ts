import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class InnerApiService {

    constructor() {
        this.api = new Subject();
    }
    api: Subject<any>;
}
