import { Component, OnInit, Input, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Attachment } from '../simple-classes/attachment';
import { WindowScrollService } from '../window-scroll.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalPictureComponent } from '../modal-picture/modal-picture.component';

@Component({
    selector: 'app-attachment',
    templateUrl: './attachment.component.html',
    styleUrls: ['./attachment.component.css']
})
export class AttachmentComponent implements OnInit, OnDestroy {
    @Input() attachment: Attachment;
    @Input() centered = true;
    @Input() min_height = '150px';
    @Input() allow_modal: false;
    constructor(
        private el: ElementRef,
        private scroll: WindowScrollService,
        private modalService: BsModalService
    ) { }

    loaded = false;
    gif_on = false;
    gif_loaded = false;
    gif_request: any;
    gif_progress: number;
    @ViewChild('attachment_div') att_div: ElementRef;

    ngOnInit() {
        if (this.attachment.type === 'gif') {
            this.scroll_sub = this.scroll.scroll.subscribe(s => {
                if (!this.gif_on) {
                    return;
                }
                if (s.top > this.att_div.nativeElement.offsetTop + this.att_div.nativeElement.clientHeight || s.bottom < this.att_div.nativeElement.offsetTop) {
                    this.turnOff();
                }
            });
        }
    }
    scroll_sub;
    ngOnDestroy() {
        if (this.scroll_sub) {
            this.scroll_sub.unsubscribe();
        }
    }
    turnOff() {
        if (this.gif_request) {
            this.gif_request.abort();
        }
        this.gif_on = false;
    }
    gifSwitch() {
        if (this.gif_on) {
            this.turnOff();
        } else {
            this.gif_on = true;
            this.gif_request = new XMLHttpRequest();
            this.gif_request.open('GET', '/useruploads/' + this.attachment.src + '.gif', true);
            this.gif_request.responseType = 'arraybuffer';
            this.gif_request.onload = (e) => {
                this.gif_loaded = true;
            };
            this.gif_request.onprogress = (e) => {
                this.gif_progress = parseInt(((e.loaded / e.total) * 100) + '');
            };
            this.gif_request.onloadstart = () => {
                this.gif_progress = 0;
            };
            this.gif_request.onabort = () => {
                this.gif_progress = 0;
            };
            this.gif_request.send();
        }
    }
    openImg() {
        if (!this.allow_modal) {
            return;
        }
        this.modalService.show(ModalPictureComponent, {
            class: "modal-picture"
        }).content.src = this.attachment.src;

    }
}
