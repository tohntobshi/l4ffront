import { Component, OnDestroy, HostBinding, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Attachment } from '../simple-classes/attachment';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';
import { AttachmentService } from '../attachment-form/attachment.service';
import { LanguageService } from '../language.service';
import { routingAnimation, postAnimation, slideFromTop, fade } from '../animations';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-create-post-form',
    templateUrl: './create-post-form.component.html',
    styleUrls: ['./create-post-form.component.css'],
    animations: [ routingAnimation, postAnimation, slideFromTop, fade ]
})
export class CreatePostFormComponent implements OnDestroy, OnInit {

    constructor(
        private backend: BackendService,
        private router: Router,
        public attSrv: AttachmentService,
        public lang: LanguageService,
        private pagetitle: Title
    ) { }
    ngOnInit() {
        this.pagetitle.setTitle(this.lang.lang.new_post);
    }
    @HostBinding('@fadeInOut') fadeInOut = true;

    form = new FormGroup({
        'title': new FormControl('',[
            Validators.required,
            Validators.maxLength(128)
        ]),
        'description': new FormControl('',[
            Validators.maxLength(5000)
        ]),
        'tags': new FormControl(),
        'hr': new FormControl(),
        'terror': new FormControl(),
        'auth_only': new FormControl()
      });
    get title() { return this.form.get('title') }
    get description() { return this.form.get('description') }

    ngOnDestroy() {
        this.attSrv.clearAttachments();
    }

    trackBySrc(index: number, attachment: Attachment): string { return attachment.src; }

    posted: boolean = false;

    post() {
        if(!this.attSrv.attachments) {
            return;
        }
        let attachments_to_send;
        this.attSrv.attachments.forEach((attachment) => {
            let attachment_to_send = {
                src: attachment.src,
                type: attachment.type
            }
            if(attachments_to_send) {
                attachments_to_send.push(attachment_to_send);
            } else {
                attachments_to_send = [attachment_to_send];
            }
        });
        let form = {
            query: "create_post",
            title: this.form.controls.title.value,
            description: this.form.controls.description.value,
            hr: (this.form.controls.hr.value === true),
            terror: (this.form.controls.terror.value === true),
            auth_only: (this.form.controls.auth_only.value === true),
            attachments: attachments_to_send,
            main_att: this.attSrv.main_att,
            tags: this.tags
        };
        this.backend.query(form).subscribe(response => {
            if (response.result) {
                this.posted = true;
                setTimeout(() => {
                    this.router.navigate(['feed']);
                }, 3000);
            }
        });
    }

    tags;
    checkTags() {
        let tags_string = this.form.controls.tags.value || "";
        let tags = tags_string.replace(/[^a-zA-Z0-9А-Яа-я ,\-]| {2,}/g, '').toLowerCase().split(",");
        tags.forEach((tag, index, tags) => {
          tags[index] = tag.trim().slice(0, 32);
        });
        tags = tags.filter(el => el.length > 1);
        let set = new Set(tags);
        this.tags = Array.from(set).slice(0, 10);
    }
}
