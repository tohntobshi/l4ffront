import { Component, ElementRef, ViewChild, OnDestroy, Input, OnInit } from '@angular/core';
import { Post } from '../simple-classes/post';
import { SharedService } from '../shared.service';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { postAnimation, slideFromTop } from '../animations';
import { WindowScrollService } from '../window-scroll.service';
import { ActivatedRoute } from '@angular/router';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css'],
    animations: [postAnimation, slideFromTop]
})

export class PostsComponent implements OnInit, OnDestroy {
    @Input() ifEmpty: string;
    @Input() postsType: string;
    @Input() searchType: string;
    @Input() searchWords: string;
    constructor(
        public shared: SharedService,
        public session: SessionService,
        public lang: LanguageService,
        private scroll: WindowScrollService,
        private route: ActivatedRoute,
        private inner: InnerApiService
    ) { }
    posts: Post[];
    @ViewChild('postsWrapper') postsWrapper: ElementRef;

    ngOnInit() {
        this.inner_api_sub = this.inner.api.subscribe(e => {
            switch(e.query) {
                case 'clean_post_votes':
                    this.cleanCurrentUserProps();
                    break;
                case 'vote_post':
                    if(!this.posts) {
                        return;
                    }
                    this.applyVotes(e.id, e.result);
                    break;
                case 'fav_post':
                    if(!this.posts) {
                        return;
                    }
                    this.applyFav(e.id, e.result)
                    break;
            }
        });
        this.startPage();
        this.scroll_sub = this.scroll.scroll.subscribe(s => {
            if (this.session.session && this.session.session.scrolltype === 'pagination') {
                return;
            }
            if (!this.posts) {
                return;
            }
            if (this.postsWrapper.nativeElement.clientHeight < s.bottom + 500) {
                this.fetchPosts(false, false);
            }
        })
    }
    inner_api_sub;
    scroll_sub;
    ngOnDestroy() {
        if (this.scroll_sub) {
            this.scroll_sub.unsubscribe();
        }
        this.inner_api_sub.unsubscribe();
    }
    trackByPosts(index: number, post: Post): number { return post.id }

    startPage() {
        this.fetchPosts(true, true);
    }
    nextPage() {
        this.fetchPosts(true, false);
    }
    isFetchPostsProcessing = false;
    fetchPosts(new_page: boolean, reset_id: boolean) {
        if (this.isFetchPostsProcessing) {
            return;
        }
        if (this.postsType === "search" && (!this.searchType || !this.searchWords)) {
            return;
        }
        if (this.route.children.length > 0) {
            return;
        }
        this.isFetchPostsProcessing = true;
        let last_id = reset_id ? 0 : this.posts[this.posts.length - 1].sep_id;
        if (new_page) {
            let posts: Post[];
            this.posts = posts;
        }
        this.shared.fetchPosts(this.postsType, last_id, this.searchType, this.searchWords).subscribe(r => {
            if (!r) {
                this.isFetchPostsProcessing = false;
                return;
            }
            if (new_page) {
                window.scrollTo(0, 0);
                this.posts = r;
            } else {
                this.posts.push(...r);
            }
            this.isFetchPostsProcessing = false;
        });
    }
    vote(id: number, vote: boolean) {
        this.shared.vote(id, vote, 'post').subscribe(r => {
            this.applyVotes(id, r);
        })
    }
    applyVotes(id: number, r: boolean) {
        let index = this.posts.findIndex(el => el.id === id);
        switch (true) {
            case this.posts[index].currentuserrate === null:
                if (r === true) {
                    this.posts[index].ratingup++
                } else if (r === false) {
                    this.posts[index].ratingdown++
                }
                break;
            case this.posts[index].currentuserrate === true:
                if (r === null) {
                    this.posts[index].ratingup--
                } else if (r === false) {
                    this.posts[index].ratingup--
                    this.posts[index].ratingdown++
                }
                break;
            case this.posts[index].currentuserrate === false:
                if (r === null) {
                    this.posts[index].ratingdown--
                } else if (r === true) {
                    this.posts[index].ratingup++
                    this.posts[index].ratingdown--
                }
                break;
        }
        this.posts[index].currentuserrate = r;
    }
    applyFav(id: number, r: boolean) {
        let index = this.posts.findIndex(el => el.id === id);
        if (this.posts[index].currentusersaved && !r) {
            this.posts[index].saved--;
        } else if (!this.posts[index].currentusersaved && r) {
            this.posts[index].saved++;
        }
        this.posts[index].currentusersaved = r;
    }
    addToFav(id: number) {
        this.shared.addToFav(id, 'post').subscribe(r => {
            this.applyFav(id, r);
        })
    }
    cleanCurrentUserProps() {
        if(this.posts) {
            this.posts.forEach((post) => {
                post.currentuserrate = undefined;
                post.currentusersaved = undefined;
            });
        }
    }
}
