import { Component } from '@angular/core';
import { BackendService } from '../backend.service';
import { SessionService } from '../session.service';
import { User } from '../simple-classes/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LanguageService } from '../language.service';
import { SharedService } from '../shared.service';

@Component({
    selector: 'app-profile-settings',
    templateUrl: './profile-settings.component.html',
    styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent {

    constructor(
        private backend: BackendService,
        public session: SessionService,
        public lang: LanguageService,
        private shared: SharedService
    ) { }

    isDisSetOpened = false;
    isTagOpened = false;
    isSubsOpened = false;
    isIgnoredOpened = false;
    isSecurityOpened = false;

    subscriptions: User[];
    openSubscriptions() {
        if (this.isSubsOpened) {
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        } else {
            let form = {
                query: "subscribed_users"
            }
            this.backend.query(form).subscribe(response => {
                if(response.result) {
                    this.subscriptions = response.data.subscribed_users;
                }
            })
            this.isIgnoredOpened = false;
            this.isSubsOpened = true;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        }
    }
    deleteSubscription(username: string) {
        this.shared.subscribe(username).subscribe(r => {
            if (r === false && this.subscriptions) {
                this.subscriptions.splice(this.subscriptions.findIndex(el => el.username === username), 1);
            }
        })
    }

    ignored: User[];
    openIgnored() {
        if (this.isIgnoredOpened) {
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        } else {
            let form = {
                query: "ignored_users"
            }
            this.backend.query(form).subscribe(response => {
                if(response.result) {
                    this.ignored = response.data.ignored_users;
                }
            })
            this.isIgnoredOpened = true;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        }
    }
    deleteIgnored(username: string) {
        this.shared.ignore(username).subscribe(r => {
            if (r === false && this.ignored) {
                this.ignored.splice(this.ignored.findIndex(el => el.username === username), 1);
            }
        })
    }

    numPosts: number;
    scrollType: string;
    show_terror: boolean;
    openDisplaySettings() {
        if (this.isDisSetOpened) {
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        } else {
            this.numPosts = this.session.session.numposts;
            this.scrollType = this.session.session.scrolltype;
            this.show_terror = this.session.session.show_terror;
            this.isDisSetOpened = true;
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        }
    }
    applySettings() {
        let form = {
            query: "apply_settings",
            numposts: this.numPosts,
            scrolltype: this.scrollType,
            show_terror: (this.show_terror === true)
        }
        this.backend.query(form).subscribe(response => {
            if (response.result === true) {
                this.session.session.numposts = this.numPosts;
                this.session.session.scrolltype = this.scrollType;
                this.session.session.show_terror = this.show_terror;
            }
        })
    }

    tags: string[];
    openTagSettings() {
        if (this.isTagOpened) {
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        } else {
            this.isSecurityOpened = false;
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            let form = {
                query: "blocked_tags"
            }
            this.backend.query(form).subscribe(response => {
                if(response.result) {
                    this.tags = response.data.blocked_tags;
                }
            })
            this.isTagOpened = true;
        }
    }
    unlockTag(tag: string) {
        let formdata = new FormData();
        let form = {
            query: "unlock_tag",
            tag: tag
        }
        this.backend.query(form).subscribe(response => {
            if (response.result === true) {
                let i = this.tags.indexOf(tag);
                this.tags.splice(i, 1);
            }
        })
    }

    change_password_form: FormGroup;
    active_sessions: number;
    alert;
    get old_pwd() { return this.change_password_form.get('old_pwd') }
    get new_pwd() { return this.change_password_form.get('new_pwd') }
    get new_pwd2() { return this.change_password_form.get('new_pwd2') }
    openSecurity() {
        if (this.isSecurityOpened) {
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isDisSetOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = false;
        } else {
            let form = {
                query: "active_sessions"
            }
            this.backend.query(form).subscribe(response => {
                if (!response.result) {
                    return;
                }
                this.active_sessions = response.data.active_sessions;
            })
            this.isDisSetOpened = false;
            this.isIgnoredOpened = false;
            this.isSubsOpened = false;
            this.isTagOpened = false;
            this.isSecurityOpened = true;
            this.change_password_form = new FormGroup({
                'old_pwd': new FormControl('', [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(32)
                ]),
                'new_pwd': new FormControl('', [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(32),
                ]),
                'new_pwd2': new FormControl('', [
                    Validators.required
                ])
            });
        }
    }
    comparePasswords() {
        if (!this.change_password_form) {
            return false;
        }
        return this.new_pwd.value == this.new_pwd2.value ? false : true;
    }
    changePassword() {
        let form = {
            query: "change_password",
            old_password: this.old_pwd.value,
            new_password: this.new_pwd.value
        }
        this.backend.query(form).subscribe(response => {
            this.change_password_form.reset();
            if (response.result) {
                var alert = {
                    type: "success",
                    msg: this.lang.lang.pwd_changed
                }
            } else if (response.error && response.error === 'wrong_old_password') {
                var alert = {
                    type: "danger",
                    msg: this.lang.lang.wrong_password
                }
            } else {
                return;
            }
            this.alert = alert;
            setTimeout(() => {
                this.alert = undefined;
            }, 5000);
        })
    }
}
