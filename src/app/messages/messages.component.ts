import { Component, OnInit, HostBinding } from '@angular/core';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { routingAnimation } from '../animations';
import { Title } from '@angular/platform-browser';
import { InnerApiService } from '../inner-api.service';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.css'],
    animations: [ routingAnimation ]
})
export class MessagesComponent implements OnInit {

    constructor(
        public session: SessionService,
        public lang: LanguageService,
        private title: Title,
        private inner: InnerApiService
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;

    ngOnInit() {
        this.session.messCounterReset();
        this.title.setTitle(this.lang.lang.messages);
    }
    refresh() {
        this.inner.api.next({
            query: "comments_start_page"
        })
    }
}
