import { Component, OnInit, HostBinding } from '@angular/core';
import { SessionService } from '../session.service';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';
import { Complaint } from '../simple-classes/complaint';
import { LanguageService } from '../language.service';
import { routingAnimation } from '../animations';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-complaints',
    templateUrl: './complaints.component.html',
    styleUrls: ['./complaints.component.css'],
    animations: [ routingAnimation ]
})
export class ComplaintsComponent implements OnInit {

    constructor(
        private session: SessionService,
        private backend: BackendService,
        private router: Router,
        public lang: LanguageService,
        private title: Title
    ) { }
    @HostBinding('@fadeInOut') fadeInOut = true;
    ngOnInit() {
        if (!this.session.session || this.session.session.status != 'admin') {
            this.router.navigate(['']);
            return;
        }
        this.title.setTitle(this.lang.lang.complaints);
        this.fetchComplaints();
    }
    complaints: Complaint[];
    fetchComplaints() {
        let form = {
            query: "fetch_complaints",
        }
        this.backend.query(form).subscribe(res => {
            if (!res.result) {
                let complaints: Complaint[];
                this.complaints = complaints;
                return;
            }
            this.complaints = res.data.complaints;
        })
    }
    deleteComplaint(complaint: Complaint) {
        let type = complaint.comment ? 'comment' : 'post';
        let id = complaint.comment ? complaint.comment : complaint.post;
        let form = {
            query: "delete_complaint",
            id: id,
            type: type
        }
        this.backend.query(form).subscribe(res => {
            if (!res.result) {
                return;
            }
            let index = type === 'post' ? this.complaints.findIndex(el => el.post === id) : this.complaints.findIndex(el => el.comment === id);
            if (index === -1) {
                return;
            }
            this.complaints.splice(index, 1);
        })
    }
}
