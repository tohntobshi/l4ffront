import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { FeedComponent } from './feed/feed.component';
import { FavPostsComponent } from './fav-posts/fav-posts.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { FavCommentsComponent } from './fav-comments/fav-comments.component';
import { MessagesComponent } from './messages/messages.component';
import { PostComponent } from './post/post.component';
import { AttachmentFormComponent } from './attachment-form/attachment-form.component';
import { CreatePostFormComponent } from './create-post-form/create-post-form.component';
import { UsersComponent } from './users/users.component';
import { ProfileComponent } from './profile/profile.component';
import { CommentsComponent } from './comments/comments.component';
import { SearchComponent } from './search/search.component';
import { VerificationComponent } from './verification/verification.component';
import { RecreationPasswordComponent } from './recreation-password/recreation-password.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { Page404Component } from './page404/page404.component';
import { EComponent } from './e/e.component';


const appRoutes: Routes = [
  { path: 'feed', component: FeedComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'favposts', component: FavPostsComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'subscriptions', component: SubscriptionsComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'favcomments', component: FavCommentsComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'messages', component: MessagesComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'search', component: SearchComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'complaints', component: ComplaintsComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'sign-up', component: SignUpComponent },
  { path: '404', component: Page404Component },
  { path: 'newpost', component: CreatePostFormComponent},
  { path: 'users', component: UsersComponent },
  { path: 'profile/:user', component: ProfileComponent, children: [
    { path: 'post/:id', component: PostComponent }
  ] },
  { path: 'verify/:code', component: VerificationComponent },
  { path: 'recreate_password/:pwd/:uid', component: RecreationPasswordComponent },
  { path: 'e', component: EComponent },
  { path: '', redirectTo: '/feed', pathMatch: 'full' },
  { path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
