import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { switchMap } from 'rxjs/operator/switchMap';
import 'rxjs/add/observable/timer';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { InnerApiService } from './inner-api.service';

@Injectable()
export class BackendService {

    constructor(
        private http: Http,
        private inner: InnerApiService
    ) {}

    processing = false;
    query2(form: Object, files = null, max_files = 0) {
        // console.log(form);
        this.processing = true;
        let formdata = new FormData();
        formdata.append('json', JSON.stringify(form));
        if (files) {
            let num_files = files.length < max_files ? files.length : max_files;
            for(var i = 0; i < num_files; i++) {
              formdata.append(i + "", files[i]);
            }
        }
        return this.http.post("/backend.php", formdata).do(() => {
            this.processing = false;
        }, () => {
            this.processing = false;
        }).map(data => {
            console.log(data['_body']);
            if (!data['_body']) {
                return {
                    result: null
                }
            }
            if (data.json().authorized === true) {
                this.inner.api.next({
                    query: "authorized"
                });
            }
            if (data.json().authorized === false) {
                this.inner.api.next({
                    query: "unauthorized"
                });
            }
            return data.json();
        }).catch(() => {
            return [{
                result: null
            }];
        });
    }
    query(form: Object, files = null, max_files = 0) {
        if (!this.processing) {
            return this.query2(form, files, max_files);
        } else {
            return Observable.timer(500).switchMap(() => {
                return this.query(form, files, max_files);
            })
        }

    }
}
