import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { User } from './simple-classes/user';
import { ActivatedRoute } from '@angular/router';
import { InnerApiService } from './inner-api.service';

@Injectable()
export class SessionService {

    constructor(
        private backend: BackendService,
        private route: ActivatedRoute,
        private inner: InnerApiService
    ) {
        this.inner.api.subscribe(e => {
            if (e.query === 'unauthorized' && this.session) {
                this.cleanSession();
            }
            // if (e.query === 'authorized' && !this.session) {
            //     this.checkSession();
            // }
        })
    }
    public session: User;

    get upic() {
        if(this.session.userpic) {
            return "/userpics/" + this.session.userpic + ".jpg";
        } else { return "/userpics/default.jpg"; }
    }

    checkSession(){
        let form = {
            query: 'check_session'
        }
        this.backend.query(form).subscribe(response => {
            if (response.result) {
              this.session = response.data.session;
            }
        })
    }
    cleanSession() {
        let session: User;
        this.session = session;
        this.message_counter = 0;
        this.inner.api.next({
            query: "clean_post_votes"
        })
    }
    logOut(everywhere = false) {
        let form = {
            query: everywhere ? 'log_out_everywhere' : 'log_out'
        }
        this.backend.query(form).subscribe(() => {
            this.checkSession();
        })
    }
    message_counter: number;
    checkMessages() {
        if(!this.session) {
            return;
        }
        if (this.route.children[0].children.length > 0) {
            return;
        }
        let form = {
            query: "check_new_messages"
        }
        this.backend.query(form).subscribe(response => {
            if (!response.result) {
                return;
            }
            this.message_counter = response.data.new_messages;
        })
    }
    messCounterReset() {
        if (!this.session) {
            return;
        }
        this.message_counter = 0;
        let form = {
            query: 'reset_messages'
        }
        this.backend.query(form).subscribe();
    }
}
