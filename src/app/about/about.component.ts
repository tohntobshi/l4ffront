import { Component } from '@angular/core';
import { LanguageService } from '../language.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css']
})
export class AboutComponent {

    constructor(
        public lang: LanguageService,
        public modalRef: BsModalRef
    ) { }

}
