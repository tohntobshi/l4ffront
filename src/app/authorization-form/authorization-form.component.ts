import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BackendService } from '../backend.service';
import { SessionService } from '../session.service';
import { LanguageService } from '../language.service';
import { SharedService } from '../shared.service';
import { NotificationComponent } from '../notification/notification.component';
import { RecreationPasswordFormComponent } from '../recreation-password-form/recreation-password-form.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

declare var VK:any;
declare var gapi: any;

@Component({
    selector: 'app-authorization-form',
    templateUrl: './authorization-form.component.html',
    styleUrls: ['./authorization-form.component.css']
})
export class AuthorizationFormComponent implements OnInit, OnDestroy {
    @Input() inline: boolean;
    log_in_form: FormGroup;
    constructor(
        private backend: BackendService,
        public session: SessionService,
        public lang: LanguageService,
        public shared: SharedService,
        public modalRef: BsModalRef,
        private modalService: BsModalService,
        private router: Router
    ) {}
    openCaptcha = false;
    ngOnInit() {
        this.google_check_interval = setInterval(() => {
            if (!this.google_id_token) {
                return;
            }
            this.googleAuth2(this.google_id_token);
            this.google_id_token = undefined;
        }, 300);
        this.log_in_form = new FormGroup({
            'username': new FormControl('', [
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(64)
            ]),
            'password': new FormControl('', [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(32)
            ])
        });
        setTimeout(()=> {
            this.openCaptcha = true;
        }, 1000);
    }
    ngOnDestroy() {
        clearInterval(this.google_check_interval);
    }
    remember = false;
    @ViewChild('captchaRef') captchaRef;
    submit(captcha: string){
        let form = {
            query: 'log_in',
            username: this.log_in_form.controls.username.value,
            password: this.log_in_form.controls.password.value,
            remember: this.remember,
            captcha: captcha
        };
        this.backend.query(form).subscribe(response => {
            this.captchaRef.reset();
            if (!response.result) {
                if (response.error != 'wrong_password') {
                    this.log_in_form.reset();
                } else {
                    this.log_in_form.controls.password.reset();
                }
                this.onAuthError(response.error);
                return;
            }
            this.session.session = response.data.session;
            this.session.checkMessages();
            this.modalRef.hide();
        });
    };
    vkAuth() {
        VK.Auth.login(response => {
            if (!response || response.status != 'connected') {
                return;
            }
            let form = {
                query: "log_in_vk",
                remember: this.remember,
                expire: response.session.expire,
                mid: response.session.mid,
                secret: response.session.secret,
                sid: response.session.sid,
                sig: response.session.sig,
            }
            this.backend.query(form).subscribe(r => {
                if (!r.result) {
                    if (r.error && r.error === "user_not_exist") {
                        this.modalRef.hide();
                        this.router.navigate(['sign-up', {reg: 'vk'}]);
                        return;
                    }
                    if (r.error && r.error === "banned") {
                        this.modalRef.hide();
                        let bannedalert = this.modalService.show(NotificationComponent);
                        bannedalert.content.title = this.lang.lang.authorization;
                        bannedalert.content.message = this.lang.lang.profile_banned;
                    }
                    return;
                }
                this.session.session = r.data.session;
                this.session.checkMessages();
                this.modalRef.hide();
            });
        })
    }
    googleAuth() {
        gapi.auth2.authorize({
            client_id: this.shared.google_client_id,
            scope: 'openid',
            response_type: 'id_token'
        }, response => {
            if (response.error) {
                return;
            }
            this.google_id_token = response.id_token;
        })
    }
    googleAuth2(id_token) {
        let form = {
            query: 'log_in_google',
            remember: this.remember,
            id_token: id_token
        }
        this.backend.query(form).subscribe(r => {
            if (!r.result) {
                if (r.error && r.error === "user_not_exist") {
                    this.modalRef.hide();
                    this.router.navigate(['sign-up', {reg: 'google'}]);
                    return;
                }
                if (r.error && r.error === "banned") {
                    this.modalRef.hide();
                    let bannedalert = this.modalService.show(NotificationComponent);
                    bannedalert.content.title = this.lang.lang.authorization;
                    bannedalert.content.message = this.lang.lang.profile_banned;
                }
                return;
            }
            this.session.session = r.data.session;
            this.session.checkMessages();
            this.modalRef.hide();
        })
    }
    google_check_interval;
    google_id_token: string;


    auth_alert;
    onAuthError(e) {
        if (!e) {
            return;
        }
        switch (e) {
            case 'wrong_password':
                this.auth_alert = this.lang.lang.wrong_password;
                break;
            case 'user_not_exist':
                this.auth_alert = this.lang.lang.uname_not_exist;
                break;
            case 'banned':
                this.modalRef.hide();
                let bannedalert = this.modalService.show(NotificationComponent);
                bannedalert.content.title = this.lang.lang.authorization;
                bannedalert.content.message = this.lang.lang.profile_banned;
                break;
            case 'new':
                this.modalRef.hide();
                let newalert = this.modalService.show(NotificationComponent);
                newalert.content.title = this.lang.lang.authorization;
                newalert.content.message = this.lang.lang.activate_profile;
                break;
        }
        setTimeout(() => {
            this.auth_alert = undefined;
        }, 5000);
    }
    forgotPwd() {
        this.modalRef.hide();
        this.modalService.show(RecreationPasswordFormComponent, {
            class: 'modal-sm'
        });
    }
}
